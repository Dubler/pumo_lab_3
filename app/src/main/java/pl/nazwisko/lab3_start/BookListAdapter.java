package pl.nazwisko.lab3_start;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;

import pl.nazwisko.lab3_start.models.Book;

/**
 * Created by luke on 03.04.17.
 */

public class BookListAdapter extends RecyclerView.Adapter<BookListAdapter.ViewHolder> {

    private List<Book> mDataset;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView title;
        public TextView author;
        public TextView date;

        public ViewHolder(View v) {
            super(v);
            title = (TextView) v.findViewById(R.id.textView8);
            author = (TextView) v.findViewById(R.id.textView9);
            date = (TextView) v.findViewById(R.id.textView10);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public BookListAdapter (List<Book> myDataset) {
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public BookListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View view = inflater.inflate(R.layout.books_list_element, parent, false);
        viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Book book = mDataset.get(position);
        if (book != null) {
            holder.title.setText(book.getTitle());
            holder.author.setText(book.getAuthor());
            SimpleDateFormat formatedDate = new SimpleDateFormat ("dd-MM-yyyy");
            holder.date.setText(formatedDate.format(book.getDate()));
        }

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}