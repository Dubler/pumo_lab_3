package pl.nazwisko.lab3_start;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import pl.nazwisko.lab3_start.models.Book;
import pl.nazwisko.lab3_start.models.Comment;

public class CommentsListActivity extends AppCompatActivity {

    private RecyclerView commentsListRecyclerView;
    private RecyclerView.Adapter commentsListAdapter;
    private RecyclerView.LayoutManager commentsListLayoutManager;

    private List<Comment> comments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments_list);

        File file = new File(getApplicationContext().getFilesDir(), "comments.txt");
        comments = new ArrayList<>();

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null) {
                comments.add(new Comment(line));
            }
            br.close();
        }
        catch (IOException e) {
            Log.e("BookMaster",e.toString());
        }
        for(Comment comment: comments) {
            Log.d("CommentsListActivity", comment.toString());
        }

        commentsListRecyclerView = (RecyclerView) findViewById(R.id.comments_list_recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView

        // use a linear layout manager
        commentsListLayoutManager = new LinearLayoutManager(this);
        commentsListRecyclerView.setLayoutManager(commentsListLayoutManager);

        // specify an adapter (see also next example)
        commentsListAdapter = new CommentListAdapter(comments);
        commentsListRecyclerView.setAdapter(commentsListAdapter);
    }
}
