package pl.nazwisko.lab3_start;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;

import pl.nazwisko.lab3_start.models.Book;
import pl.nazwisko.lab3_start.models.Comment;

/**
 * Created by luke on 03.04.17.
 */

public class CommentListAdapter extends RecyclerView.Adapter<CommentListAdapter.ViewHolder> {

    private List<Comment> mDataset;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView comment;

        public ViewHolder(View v) {
            super(v);
            comment = (TextView) v.findViewById(R.id.comment_text_view);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public CommentListAdapter(List<Comment> myDataset) {
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public CommentListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View view = inflater.inflate(R.layout.comment_list_element, parent, false);
        viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Comment comment = mDataset.get(position);
        if (comment != null) {
            holder.comment.setText(comment.getValue());
        }

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}