package pl.nazwisko.lab3_start.models;

/**
 * Created by sszwaczyk on 28.03.17.
 */

public class Comment {

    private String value;

    public Comment() {

    }

    public Comment(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
