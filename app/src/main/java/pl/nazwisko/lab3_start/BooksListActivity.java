package pl.nazwisko.lab3_start;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pl.nazwisko.lab3_start.models.Book;


public class BooksListActivity extends AppCompatActivity {


    private RecyclerView booksListRecyclerView;
    private RecyclerView.Adapter booksListAdapter;
    private RecyclerView.LayoutManager booksListLayoutManager;

    private List<Book> books;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_books_list);


        Book b = new Book("Sebastian Szwaczyk", "O obrocie sfer niebieskich", new Date(117, 10, 10));
        Book b1 = new Book("Jan Kowalski", "Szkoła programowania", new Date(112, 4, 3));
        Book b2 = new Book("Michał Ciołek", "Android dla opornych", new Date(117, 1, 1));
        Book b3 = new Book("Magdalena Gessler", "Kuchenne rewolucje", new Date(99, 11, 23));
        Book b4 = new Book("Autor5", "Tytul5", new Date(117, 10, 10));
        Book b5 = new Book("Autor6", "Tytul6", new Date(117, 10, 10));
        Book b6 = new Book("Autor7", "Tytul7", new Date(117, 10, 10));
        Book b7 = new Book("Autor8", "Tytul8", new Date(117, 10, 10));
        Book b8 = new Book("Autor9", "Tytul9", new Date(117, 10, 10));
        Book b9 = new Book("Autor10", "Tytul10", new Date(117, 10, 10));
        Book b10 = new Book("Autor11", "Tytul11", new Date(117, 10, 10));
        Book b11 = new Book("Autor12", "Tytul12", new Date(117, 10, 10));

        books = new ArrayList<>();
        books.add(b);
        books.add(b1);
        books.add(b2);
        books.add(b3);
        books.add(b4);
        books.add(b5);
        books.add(b6);
        books.add(b7);
        books.add(b8);
        books.add(b9);
        books.add(b10);
        books.add(b11);



        for(Book book: books) {
            Log.d("BooksListActivity", book.toString());
        }

        booksListRecyclerView = (RecyclerView) findViewById(R.id.books_list_recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        booksListRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        booksListLayoutManager = new LinearLayoutManager(this);
        booksListRecyclerView.setLayoutManager(booksListLayoutManager);

        // specify an adapter (see also next example)
        booksListAdapter = new BookListAdapter(books);
        booksListRecyclerView.setAdapter(booksListAdapter);

    }
}
