package pl.nazwisko.lab3_start;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class AddCommentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_comment);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void addCommentPressed(View view) {
        EditText editText = (EditText) findViewById(R.id.editText2);
        String commentText = editText.getText().toString().trim();
        if (commentText.isEmpty()) {
            Toast.makeText(getApplicationContext(), R.string.comment_content_is_required, Toast.LENGTH_LONG).show();
            return;
        }
        File file = new File(getApplicationContext().getFilesDir(), "comments.txt");
        try {
            FileWriter fw = new FileWriter(file,true);
            fw.append(commentText + "\n");
            fw.close();
        } catch (IOException e){
            Log.e("BookMaster", "File write exception");
        }
        Toast.makeText(getApplicationContext(), R.string.comment_added, Toast.LENGTH_LONG).show();
    }
}
